import React from 'react';
import './Main.css'

const Main = props => {
    return(
        <div className="Main">
            <img className="img" src={props.picture} alt="" />
        </div>
    )
}

export default Main;