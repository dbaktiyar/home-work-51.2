import React from 'react';
import './Content.css';
import Card from '../Card/Card';

const Content = props => {
    return(
        <div className="Content">
            <h1>{props.title}</h1>
            <Card picture={props.picture1} text={props.text1} />
            <Card picture={props.picture2} text={props.text2} />
            <Card picture={props.picture3} text={props.text3} />
        </div>
    )
};

export default Content;