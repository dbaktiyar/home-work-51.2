import React, { Component } from 'react';
import './App.css';
import Header from "./Header/Header";
import Main from "./Main/Main";
import Content from "./Content/Content";
import Footer from "./Footer/Footer";

class App extends Component {
    render() {
        return (
            <div className="App">

                <Header header1="contact" header2="work" header3="about" header4="home" />

                <Main picture="../main-photo.jpg" />

                <Content title="Portfolio Heading"
                         picture1="../1.jpg" text1="p111"
                         picture2="../2.jpg" text2="p222"
                         picture3="../3.jpg" text3="p333"  />

                <Footer footer="Technology is giving life the potential to flourish like never before... Or to self destruct. Let's make a difference!"/>

            </div>
        );
    }
}

export default App;
