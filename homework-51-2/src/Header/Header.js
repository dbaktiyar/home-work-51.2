import React from 'react';
import './Header.css';

const Header = props => {
    return(
        <div className="Header">
            <ul>
                <li>
                    <a href="#">{props.header1}</a>
                </li>
                <li>
                    <a href="#">{props.header2}</a>
                </li>
                <li>
                    <a href="#">{props.header3}</a>
                </li>
                <li>
                    <a href="#">{props.header4}</a>
                </li>
            </ul>
        </div>
    )
}

export default Header;