import React from 'react';
import './Card.css';

const Card = props => (
    <div className="Card">
        <img src={props.picture} alt="" />
        <p>{props.text}</p>
    </div>
);

export default Card;